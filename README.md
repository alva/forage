# Final Forage

I had a dream about a GBA game called _Fantastic Forage_, but Final Forage sounds funnier.
In the dream you play a elfin looking girl, who fights her way through various stages. I'll change this to a sami hunter/gatherer instead of myself.
The story was something about foraging for berries of increasing elusiveness.
On the first stage maybe you’re searching for blueberries, and at a later stage it’s cloudberries.
She is armed with some sort of boomerang and some pretty intricate gameplay mechanics.

## Mechanics

At the start of the game, you only know how to throw it, and have to go pick it up.
As the game progresses, you learn new ways of using it:

- Throw it so that it comes back.
- *Charge Throw*: hold the button to throw it faster—the longer you hold, the faster it goes.
- *Guided Throw*: in some kind of inventory screen, there was a 15×10 grid representing the 240×160 GBA screen, where you can set up a path for the
  weapon to follow. I'll diverge from the dream and put it on the main screen
  instead.

The last one is used to great effect in boss fights, where you have to:

1. Spot the boss’s pattern and replicate it.
2. Combine this with charge throw to get the correct pattern at the right speed.
3. Other power-ups that I don’t remember.

## Story

I don’t really know what the story was about, I’ll make something up to explain what all the berries are for.
Also I don’t want to kill animals so if the enemies are animals, they will be under the influence of some evil force, and knocked unciscious.
Maybe the background is GMO, climate change, and evil corporations making the forest fucked up.
